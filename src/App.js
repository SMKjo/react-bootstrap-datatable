import React, { Component } from "react";
import Datatable from "react-bs-datatable"; 
import "bootstrap/dist/css/bootstrap.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: null
    };
    this.header = [
      { title: "Name", prop: "name", sortable: true, filterable: true },
      { title: "User Name", prop: "username", sortable: true,filterable: true},
      { title: "Phone", prop: "phone", sortable: true, filterable: true },
      { title: "Website", prop: "website", sortable: true, filterable: true }
    ];
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then(res => res.json())
      .then(result => {
          this.setState({
            isLoaded: true,
            items: result
          });
         }).catch(error => 
        this.setState({
          isLoaded: true,
          error: error
        })
      );
      
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      var tableBodyItems = items;
      return (
        <div className="container">
          <Datatable
            tableHeaders={this.header}
            tableBody={tableBodyItems}
            keyName="userTable"
            tableClass="striped hover responsive"
            rowsPerPage={5}
            rowsPerPageOption={[3, 5, 8, 10]}
            initialSort={{ prop: "username", isAscending: true }}
          />
        </div>
      );
    }
  }
}
export default App;